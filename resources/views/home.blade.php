@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }} || <a href="">Export Pdf</a></div>

                    <div class="card-body">
                        {{-- <h2>Tools Visualization</h2> --}}
                        <a href="{{ route('export') }}" class="btn btn-primary">Export PDF</a>
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Pie Chart</h4>
                                <canvas id="pieChart" width="300" height="300"></canvas>
                            </div>
                            <div class="col-md-4">
                                <h4>Bar Chart</h4>
                                <canvas id="barChart" width="300" height="300"></canvas>
                            </div>
                            <div class="col-md-4">
                                <h4>Line Chart</h4>
                                <canvas id="lineChart" width="300" height="300"></canvas>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-4">
                                <h4>RPM Chart</h4>
                                <canvas id="rpmChart" width="300" height="300"></canvas>
                            </div>
                            <div class="col-md-4">
                                <h4>Doughnut Chart</h4>
                                <canvas id="bulatChart" width="300" height="300"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        var labels = {!! json_encode($categoryNames) !!};
        var toolCounts = {!! json_encode($toolCounts) !!};

        var pieData = {
            labels: labels,
            datasets: [{
                data: toolCounts,
                backgroundColor: [
                    'red',
                    'blue',
                    'green',
                    // Add more colors here
                ]
            }]
        };

        var pieOptions = {
            responsive: true
        };

        var pieChart = new Chart(document.getElementById("pieChart"), {
            type: 'pie',
            data: pieData,
            options: pieOptions
        });

        var barData = {
            labels: labels,
            datasets: [{
                label: 'Tool Count',
                data: toolCounts,
                backgroundColor: 'blue',
            }]
        };

        var barOptions = {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true,
                    stepSize: 1,
                }
            }
        };

        var barChart = new Chart(document.getElementById("barChart"), {
            type: 'bar',
            data: barData,
            options: barOptions
        });

        var lineData = {
            labels: labels,
            datasets: [{
                label: 'Tool Count',
                data: toolCounts,
                borderColor: 'green',
                fill: false,
            }]
        };

        var lineOptions = {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true,
                    stepSize: 1,
                }
            }
        };

        var lineChart = new Chart(document.getElementById("lineChart"), {
            type: 'line',
            data: lineData,
            options: lineOptions
        });

        var rpmData = {
            labels: labels,
            datasets: [{
                label: 'RPM',
                data: toolCounts,
                borderColor: 'orange',
                fill: false,
            }]
        };

        var rpmOptions = {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true,
                }
            }
        };

        var rpmChart = new Chart(document.getElementById("rpmChart"), {
            type: 'line',
            data: rpmData,
            options: rpmOptions
        });

        var bulatData = {
            labels: labels,
            datasets: [{
                label: 'Bulat',
                data: toolCounts,
                borderColor: 'purple',
                fill: false,
            }]
        };

        var bulatOptions = {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true,
                }
            }
        };

        var bulatChart = new Chart(document.getElementById("bulatChart"), {
            type: 'doughnut',
            data: bulatData,
            options: bulatOptions
        });
    </script>
@endsection
