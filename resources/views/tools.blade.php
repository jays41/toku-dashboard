@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Tools') }} | <a href="{{ route('tools.export') }}"
                            class="text-success">Export to Excel</a></div>
                    <form action="{{ route('tools.import') }}" method="POST" enctype="multipart/form-data" class="m-3">
                        @csrf
                        <div class="mb-3">
                            <label for="file" class="form-label">Import Excel</label>
                            <input type="file" class="form-control" name="file" accept=".xlsx">
                        </div>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </form>

                    <div class="card-body">
                        <table class="table" id="toolsTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category</th>
                                    <th>Name</th>
                                    {{-- <th>Created At</th>
                                    <th>Updated At</th> --}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#toolsTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('tools.datatables') }}', // Replace with your route
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'category.name',
                        name: 'category.name'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    // {
                    //     data: 'created_at',
                    //     name: 'created_at'
                    // },
                    // {
                    //     data: 'updated_at',
                    //     name: 'updated_at'
                    // }
                ]
            });
        });
    </script>
@endsection
