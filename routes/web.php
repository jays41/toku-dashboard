<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ToolsController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
Route::get('/categories/datatables', [CategoryController::class, 'datatables'])->name('categories.datatables');
Route::get('tools', [ToolsController::class, 'index'])->name('tools.index');
Route::get('tools/datatables', [ToolsController::class, 'datatables'])->name('tools.datatables');
Route::get('tools/export', [ToolsController::class, 'export'])->name('tools.export');
Route::post('tools/import', [ToolsController::class, 'import'])->name('tools.import');
Route::get('/export-pdf', [HomeController::class, 'exportPDF'])->name('export');


