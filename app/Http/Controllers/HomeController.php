<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Tools;
use PDF;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::with('tools')->get();

        $categoryNames = $categories->pluck('name');
        $toolCounts = $categories->map(function ($category) {
            return $category->tools->count();
        });

        return view('home', compact('categoryNames', 'toolCounts'));
    }

    public function exportPDF()
    {
        $categoryNames = Category::pluck('name')->toArray();
        $toolCounts = Tools::groupBy('category_id')->pluck('category_id', DB::raw('count(*)'))->toArray();

        $data = [
            'categoryNames' => $categoryNames,
            'toolCounts' => $toolCounts,
        ];

        $pdf = PDF::loadView('export', $data);

        return $pdf->download('tools_report.pdf');
    }
}
