<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Tools;
use App\Models\Category;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ToolsExport;
use App\Imports\ToolsImport;

class ToolsController extends Controller
{
    public function index()
    {
        return view('tools');
    }

    public function datatables()
    {
        $tools = Tools::with('category')->select(['id', 'category_id', 'name', 'created_at', 'updated_at']);

        return DataTables::of($tools)
        ->addColumn('category_name', function($tool) {
            return $tool->category->name;
        })
        ->toJson();
    }

    public function export()
    {
        return Excel::download(new ToolsExport, 'tools.xlsx');
    }

    public function import(Request $request)
    {
        $file = $request->file('file');

        Excel::import(new ToolsImport, $file);

        return redirect()->route('tools.index')->with('success', 'Data imported successfully.');
    }
}
