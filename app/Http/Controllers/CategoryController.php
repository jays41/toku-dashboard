<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return view('categories');
    }

    public function datatables()
    {
        $categories = Category::select(['id', 'name', 'created_at', 'updated_at']);

        return DataTables::of($categories)->toJson();
    }
}
