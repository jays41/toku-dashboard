<?php

namespace App\Imports;

use App\Models\Tools;
use Maatwebsite\Excel\Concerns\ToModel;

class ToolsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Tools([
            // 'id' => $row[0], // Assuming the first column contains the ID
            'category_id' => $row[1], // Assuming the second column contains the category ID
            'name' => $row[2], // Assuming the third column contains the tool name
            // 'other_field' => $row[3], // Uncomment and add more fields if needed
        ]);
    }
}
