<?php

namespace App\Exports;

use App\Models\Tools;
use Maatwebsite\Excel\Concerns\FromCollection;

class ToolsExport implements FromCollection
{
    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {
    //     return Tools::all();
    // }

        /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Tools::select('name', 'category_id')->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Name',
            'Category ID',
        ];
    }
}
